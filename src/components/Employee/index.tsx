import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import { ScreenParamsList } from '../../navigators/paramsList';
import { IEmployee } from '../../types/employee';
import styles from './styles';
import { Card } from '../Card';

type EmployeesStackProp = StackNavigationProp<ScreenParamsList, 'Employees'>;

interface EmployeeProps {
  item: IEmployee;
}

export const Employee = (props: EmployeeProps) => {
  const { item } = props;
  const { navigate } = useNavigation<EmployeesStackProp>();

  const goToEmployeeDetail = () => {
    navigate('EmployeeDetail', { employee: item });
  };

  const renderItem = () => {
    if (Platform.OS === 'android') {
      return <Card onPress={goToEmployeeDetail} item={item} />;
    }
    return (
      <TouchableOpacity
        style={styles.employeeItem}
        onPress={goToEmployeeDetail}>
        <View style={styles.employeeInfo}>
          <Text>
            {item.firstname} {item.lastname}
          </Text>
          <Text>{item.email}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return renderItem();
};
