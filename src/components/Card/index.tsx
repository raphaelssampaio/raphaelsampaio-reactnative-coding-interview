import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import { IEmployee } from '../../types/employee';

interface CardProps {
  onPress: () => void;
  item: IEmployee;
  cardChild?: Element;
}

export function Card(props: CardProps) {
  const { item, onPress, cardChild } = props;
  const { firstname, lastname, email, phone } = item;
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <View>
        {phone && <Text>{phone}</Text>}
        <Text style={styles.text}>
          {firstname} {lastname}
        </Text>
        <Text style={styles.email}>{email} </Text>
        {cardChild}
      </View>
    </TouchableOpacity>
  );
}
