import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  text: {
    color: 'red',
    fontSize: 14,
    marginBottom: 4,
  },
  email: {
    color: 'blue',
    fontSize: 12,
  },
  button: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    backgroundColor: 'grey',
    marginHorizontal: 16,
    shadowColor: 'black',
    shadowOffset: {
      height: 5,
      width: 2,
    },
    elevation: 5,
  },
});
