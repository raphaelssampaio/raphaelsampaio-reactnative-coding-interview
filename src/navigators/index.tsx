import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList } from './paramsList';
import UserProfile from '../screens/UserProfile';

const Stack = createStackNavigator<ScreenParamsList>();
const Drawer = createDrawerNavigator();

export function MainNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: '#6C3ECD',
        headerTitleAlign: 'center',
      }}>
      <Stack.Screen component={EmployeesScreen} name="Employees" />
      <Stack.Screen component={EmployeeDetailScreen} name="EmployeeDetail" />
    </Stack.Navigator>
  );
}

export function DrawerNavigator() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen component={MainNavigator} name="MainNavigator" />
      <Drawer.Screen component={UserProfile} name="UserProfile" />
    </Drawer.Navigator>
  );
}
